(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): www.lynda.com
(Course Name): Foundations of Programming: Design Patterns
(Course URL) : http://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Design-Patterns/135365-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 12
(Grade style): 1
(Random answers): 1
(Question): Which of the following represent a good use of a singleton design pattern?
(A): Database connection class
(B): Logging utility class
(C): Every single class 
(D): Only parent classes
(E): None of the above
(Correct): A,B
(Points): 1
(CF): Logging and database utility classes are good candidates for use of a singleton pattern.
(WF): Logging and database utility classes are good candidates for use of a singleton pattern.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): The java I/O classes use a decorator design pattern.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Java.io classes do use a decorator pattern.
(WF): Java.io classes do use a decorator pattern.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Why would you want to loosely couple objects?
(A): Because it's the cool thing to do now
(B): This is what you do with singleton pattern implementation
(C): It provides the ability to vary the design without breaking existing contracts
(D): None of the above
(Correct): B
(Points): 1
(CF): You want to loosely couple your objects to provide the ability to change design without breaking stuff.
(WF): You want to loosely couple your objects to provide the ability to change design without breaking stuff.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): A design pattern is NOT a guideline for how to solve a problem.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A design pattern IS a guideline for how to solve a problem.
(WF): A design pattern IS a guideline for how to solve a problem.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): What does the factory design pattern provide us with?
(A): A single instance of an object that gets re-used
(B): A push/pull type relationship between classes
(C): Creates objects without exposing instantiation logic to the client
(D): None of the above
(Correct): C
(Points): 1
(CF): The main purpose to use a factory design pattern is to provide the ability to create objects without exposing the instantiation to the clients.
(WF): The main purpose to use a factory design pattern is to provide the ability to create objects without exposing the instantiation to the clients.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
