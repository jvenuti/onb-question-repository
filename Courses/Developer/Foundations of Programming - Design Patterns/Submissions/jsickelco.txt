(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Foundations of Programming: Design Patterns
(Course URL) : http://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Design-Patterns/135365-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which of the following techniques best promotes loose coupling?
(A): Coding to interfaces, not classes
(B): Developing unit tests
(C): Prioritizing inheritance over composition
(D): Using polymorphism
(E): Using the new operator to instantiate objects yourself, rather than relying on a factory
(Correct): A
(Points): 1
(CF): Of the choices given, only coding to interfaces promotes loose coupling.
(WF): Of the choices given, only coding to interfaces promotes loose coupling.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12 
(Grade style): 0
(Random answers): 1
(Question): Which of the following offers a real-world example of the decorator pattern?
(A): Java's IO libraries
(B): The "synchronized" keyword
(C): Garbage collection
(D): Java's built-in Iterator interface
(E): The MVC pattern 
(Correct): A
(Points): 1
(CF): Java's IO libraries make use of the decorator pattern.
(WF): Java's IO libraries make use of the decorator pattern.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): The State pattern is quite similar to another pattern mentioned in this course. Which one?
(A): The Singleton pattern
(B): The Iterator pattern
(C): The Strategy pattern
(D): The Decorator pattern
(E): The Factory pattern
(Correct): C
(Points): 1
(CF): The State pattern, when diagrammed, is very similar to the Strategy pattern. However, the intent behind the use of the pattern is different.
(WF): The State pattern, when diagrammed, is very similar to the Strategy pattern. However, the intent behind the use of the pattern is different.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): What is the single-responsibility principle?
(A): Classes should not inherit from multiple superclasses
(B): A project should only use a single pattern
(C): Observer classes should be notified by a single Subject class
(D): Classes should have only one reason to change
(E): The Context class in the State pattern can be in only one state at a time
(Correct): D
(Points): 1
(CF): The single-responsibility principle states that classes should have only one reason to change.
(WF): The single-responsibility principle states that classes should have only one reason to change.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which access modifier should be used on the constructor of a singleton class?
(A): No modifier
(B): public
(C): private
(D): protected
(E): single
(Correct): C
(Points): 1
(CF): A Singleton's constructor should always be private; it will be called from within a class method.
(WF): A Singleton's constructor should always be private; it will be called from within a class method.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT true of patterns?
(A): They promote a powerful shared vocabulary
(B): They are the "ultimate" in re-use, as they re-use the experience of prior developers
(C): They are guidelines for design
(D): They promote code flexibility
(E): They should be used in all programming projects
(Correct): E
(Points): 1
(CF): Patterns are very useful, but they should only be used when they fit the problem at hand and the project is likely to experience some degree of change.
(WF): Patterns are very useful, but they should only be used when they fit the problem at hand and the project is likely to experience some degree of change.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a foundational concept behind most patterns?
(A): Encapsulate what varies
(B): Favor "is-a" relationships over "has-a" relationships
(C): Classes should have multiple responsibilities
(D): Favor tightly coupled classes
(E): Classes should be open for modification
(Correct): A
(Points): 1
(CF): The concept that code should "encapsulate what varies" underlies most design patterns.
(WF): The concept that code should "encapsulate what varies" underlies most design patterns.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 0
(Question): True or false, Java provides a Decorator interface to facilitate use of the Decorator pattern
(A): True
(B): False
(Correct): B
(Points): 1
(CF): False - Java has Iterator and Observable features to assist with those patterns, but Decorator does not lend itself to built-in interfaces.
(WF): False - Java has Iterator and Observable features to assist with those patterns, but Decorator does not lend itself to built-in interfaces.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): When using Java's built-in Observable class, what method or methods need to be called when notifying observers of changes?
(A): setChanged(), followed by notifyObservers()
(B): publish()
(C): notifyObservers()
(D): onChanged(), followed by alert()
(E): push()
(Correct): A
(Points): 1
(CF): To notify observers of changes, the Observable must call setChanged(), followed by notifyObservers().
(WF): To notify observers of changes, the Observable must call setChanged(), followed by notifyObservers().
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 12
(Grade style): 0
(Random answers): 1
(Question): Which of the following methods must be included in a class implementing Java's Iterator interface? 
(A): forward(), back()
(B): next(), hasNext(), remove()
(C): next(), insert(), delete()
(D): advance(), getCurrent()
(E): hasElements()
(Correct): B
(Points): 1
(CF): Java's Iterator interface requires next(), hasNext(), and remove() methods.
(WF): Java's Iterator interface requires next(), hasNext(), and remove() methods.
(STARTIGNORE)
(Hint):
(Subject): Design Patterns
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)