(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Lynda
(Course Name): Web Design Fundamentals
(Course URL): http://www.lynda.com/Web-Design-tutorials/Web-Design-Fundamentals/177837-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): JavaScript should be in script tags at the top of the page to optimize loading time.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): When possible, JavaScript should be at the bottom of the page so that it does not interfere with loading the content immediately visible to the user.
(WF): When possible, JavaScript should be at the bottom of the page so that it does not interfere with loading the content immediately visible to the user.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Which of the following does front end design focus on?
(A): The visual aspect of websites.
(B): Returning database queries.
(C): Client-side scripting.
(D): User interaction and experience.
(E): Handling complex calculations on the server.
(Correct): A, C, D
(Points): 1
(CF): Front end design focuses on the parts of a website the end-user will be interacting with.
(WF): Front end design focuses on the parts of a website the end-user will be interacting with.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Mockups should convey site functionality.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Prototypes are for previewing site functionality. Mockups are for previewing visual elements.
(WF): Prototypes are for previewing site functionality. Mockups are for previewing visual elements.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What are some main features of a CMS (Content Management System) for websites?
(A): Allow for quick creation of websites.
(B): Make it easy to manage pages and how they are navigated.
(C): Typically use a database to store content.
(D): Templates are a common feature used to standardize visual aspects.
(E): All of the above.
(Correct): E
(Points): 1
(CF): All of the answers are traits of most Content Management Systems.
(WF): All of the answers are traits of most Content Management Systems.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): "Content is king" refers to which of the following?
(A): The most important content should be front, center, and easily accessible.
(B): A website needs to have a lot of content to get noticed.
(C): Content needs to be frequently updated or your readership will dwindle.
(D): Content should only be added to the site after the "throne" (empty templates) is complete.
(E): Old content should be archived somewhere on the website in case users want to view it.
(Correct): A
(Points): 1
(CF): Users come to your site for content, so it should be easily viewable no matter what device they are on.
(WF): Users come to your site for content, so it should be easily viewable no matter what device they are on.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): In reference to web design, frameworks refer to which of the following?
(A): The combination of mockups and prototypes of a website.
(B): The phrase "content is king".
(C): A collection of HTML, CSS, and JS files that provide a foundation for website design.
(D): A pre-designed website template that can quickly be reworked.
(E): All the software a web designer uses, including a code editor, graphics editor, testing tools, and prototyping tools.
(Correct): C
(Points): 1
(CF): Web design frameworks are a collection of HTML, CSS, and JS files that speed up development by provide a foundation for structure, styling, layout, and functionality.
(WF): Web design frameworks are a collection of HTML, CSS, and JS files that speed up development by provide a foundation for structure, styling, layout, and functionality.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What are some disadvantages of using a JavaScript library?
(A): They can cause unnecessary bloating in a project.
(B): They often contain unstable, untested code.
(C): They "tie" you to the coding standards of the library's developer.
(D): They significantly slow down development.
(E): Many users end up becoming too reliant on a particular library.
(Correct): A, C, E
(Points): 1
(CF): JS libraries are usually very stable and highly tested code that can greatly speed up web development.
(WF): JS libraries are usually very stable and highly tested code that can greatly speed up web development.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What does a content strategist's primary role include?
(A): Writing content for a website.
(B): Deciding the style in which the content will be presented.
(C): Determining who is responsible for maintaining and updating content.
(D): Deciding what types of content will be on a website.
(E): Writing the code for a website.
(Correct): B, C, D
(Points): 1
(CF): A content strategist is primarily concerned with what and how content will be viewed on a website.
(WF): A content strategist is primarily concerned with what and how content will be viewed on a website.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Your manager is upset that every time he puts his phone into landscape mode, the website becomes distorted with a lot of whitespace. Looks like you need to look into which of the following?
(A): Following web standards.
(B): A more appealing visual design.
(C): Teaching your boss how to lock his phone in portrait mode.
(D): Responsive design.
(E): A new CMS.
(Correct): D
(Points): 1
(CF): Responsive design refers to fluidity in a website. It should alter its look based on screen size, orientation, locale, and other factors.
(WF): Responsive design refers to fluidity in a website. It should alter its look based on screen size, orientation, locale, and other factors.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)