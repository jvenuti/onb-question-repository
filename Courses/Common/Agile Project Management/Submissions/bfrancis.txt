(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the best description of a feature?
(A): A requirement.
(B): The outcome of completing an agile project.
(C): A feature is a small piece of functionality that has an action and a result.
(D): An item in the backlog to track bugs.
(Correct): C
(Points): 1
(CF): A feature is a small piece of functionality that has an action and a result.
(WF): A feature is a small piece of functionality that has an action and a result.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the most ideal length of time for a standup?
(A): 1 minute 
(B): 15 minutes
(C): 1 day
(D): 40 minutes
(Correct): B
(Points): 1
(CF): Ideally should be able to complete within 15 minutes.
(WF): Ideally should be able to complete within 15 minutes.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is best to try and solve issues or problems at the standup meeting.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The standup meeting is not the place to try and solve issues, rather it is simply the vehicle to let the team know there is an issue.
(WF): The standup meeting is not the place to try and solve issues, rather it is simply the vehicle to let the team know there is an issue.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What is the purpose of a burndown chart?
(A): A chart to show how much work is left to complete in a project. 
(B): Another useless chart that no one uses.
(C): Helps show if you are on schedule.
(D): Gives you the opportunity to make adustments to maximize productivity.
(Correct): A,C,D
(Points): 1
(CF): Burndown charts are useful to show work completed at any given point in time, shows if the team is on schedule, provides an opportunity to make adjustments on the fly to meet desired goals.
(WF): Burndown charts are useful to show work completed at any given point in time, shows if the team is on schedule, provides an opportunity to make adjustments on the fly to meet desired goals.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What values are true about recording lessons learned during the sprint?
(A): It's just more data to collect and throw away at the end of the sprint.
(B): It is a data that is used to fix a problem.
(C): Data that is prioritized based on feedback from the team.
(D): An opportunity for team members to discuss problems that arose during a sprint.
(Correct): B,C,D
(Points): 1
(CF): Lessons learned during the sprint process is an opportunity for team members to report a problem.   The data is collected, voted on and prioritized for resolution.
(WF): Lessons learned during the sprint process is an opportunity for team members to report a problem.   The data is collected, voted on and prioritized for resolution.
(STARTIGNORE)
(Hint):
(Subject): Adapting and Closing
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
